# font-paliari

Custom paliari font icons

[Preview icons](http://paliari.github.io/font-paliari)


### Installation

#### With Bower

    $ bower install --save font-paliari

#### With NPM

    $ npm install --save font-paliari


### Usage

Include the plugin in your application:

```html
<link rel="stylesheet" href="font-paliari/font-paliari.css">
```

And then include the *class* into html:

```html
<i class="fp fp-isse"></i>
```

Changing the font-size:

```html
<i class="fp fp-isse fp-2x"></i>
```
Available options: **fp-lg | fp-2x | fp-3x | fp-4x | fp-5x | fp-6x | fp-7x | fp-8x | fp-fw**

### Development

    $ git clone git@bitbucket.org:paliari/font-paliari.git
    $ npm install

> Note: is necessary to have **[FontCustom](https://github.com/FontCustom/fontcustom)** installed in your machine

#### Adding a new font

1. Include a **svg** file with your vector in the *vectors* folder
2. Compile the new icon

        $ gulp

3. Bumps the package version

        $ gulp bump

      > It runs **bump patch** as default

      > You can pass a parameter `gulp bump --type minor`

      > Available options: **major|minor|patch|prerelease**



### Author

[Daniel Fernando Lourusso](http://dflourusso.com.br)
