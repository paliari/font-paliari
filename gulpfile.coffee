gulp = require 'gulp'
es   = require 'event-stream'
argv = require('yargs').argv
exec = require('child_process').exec
$    = require('gulp-load-plugins')()

gulp.task 'default', ->
  exec 'fontcustom compile vectors', (err, stdout, stderr) ->
    if stdout
      $.util.log(stdout, $.util.colors.green('SUCCESS!'))
    else
      $.util.log(stderr, err, $.util.colors.red('ERROR'))
      $.util.beep()

# Sem parametro executa "bump patch"
# Ou passar o tipo desejado: "gulp bump --type minor"
# Opcoes permitidas: major|minor|patch|prerelease
gulp.task 'bump', ->
  gulp.src ['./bower.json', './package.json']
  .pipe $.bump type: argv.type
  .pipe gulp.dest './'
  .pipe $.git.commit 'bumps package version'
  .pipe $.filter 'bower.json'
  .pipe $.tagVersion(prefix: '')
